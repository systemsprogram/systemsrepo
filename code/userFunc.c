/*This file contains the functions for verifying the users access and
any other functions needed by users specifically*/
#include "userFunc.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*method for getting authentication from username,
or returns null if the account doesnt exist*/
struct auth * getCredentials(char user[MAX_SIZE], struct auth *head){
  struct auth *temp;
  temp = head;
  bool found = false;
  printf("%s\n", temp->username);
  while(temp != NULL && found == false){
    if(strcmp(temp->username, user) == 0){
      found = true;
      return(temp);
      printf("%s\n", temp->username);
    }
    temp = temp->next;
  }
  return(NULL);
}

/*method for returning the client details from client number,
or returns null if the client doesnt exist*/
struct client * getDetails(int num, struct client *head){
  struct client *temp;
  temp = head;
  while(temp != NULL){
    if(temp->clientNo == num){
      return(temp);
    }
    temp = temp->next;
  }
  return(NULL);
}
