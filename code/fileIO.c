/*These functions are used to read the data in from files and save the data
upon termination of the program*/

#include "fileIO.h"
#include "processing.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

static const char authFile[] = "Authentication.txt";
const char accountFile[] = "Accounts.txt";
const char clientFile[] = "Client_Details.txt";
const char transactionFile[] = "Transactions.txt";

/* Files to load:
1. Authentication.txt
2. Accounts.txt
3. Client_Details.txt
4. Transactions.txt*/

void assignAccounts(struct client *entry, int acc[3], struct account *head);
void sortTransactions(struct transaction **head);
struct transaction * merge(struct transaction *list1, struct transaction *list2);
void split(struct transaction *bodyList, struct transaction **front, struct transaction **back);

/*Read in method takes lists for all the files, lists defined in the
header file*/
void load_data(struct auth_list *authList, struct account_list *accountList, struct client_list *clientList, struct transaction_list *transactionList){
  FILE *reader;
  char noRead[128];

  // 1. Load Authentication
  printf("Reading %s\n", authFile);
  reader=fopen(authFile, "r");
  if(reader==NULL){
    // file not found
    perror("File read error: ");
  } else {
    fgets(noRead, sizeof(noRead), reader); // skip title line
    while(!feof(reader)){
      struct auth *loc = (struct auth*)malloc(sizeof(struct auth));
      if(fscanf(reader, "%s  %d  %d", loc->username, &loc->pin, &loc->clientNo) > 0){
        loc->next = NULL;
        //printf("%s  %d  %d\n", loc->username, loc->pin, loc->clientNo);
        if(authList->head == NULL){
          // if list is empty assign current as head
          authList->head = loc;
        } else {
          authList->tail->next = loc;
        }
        authList->tail = loc; // current is at end of list
      } else {
        free(loc);
        break; // reached EOF
      }
    }
    fclose(reader);
  }

  // 2. Load Accounts
  printf("Reading %s\n", accountFile);
  reader=NULL;
  reader=fopen(accountFile, "r");
  if(reader==NULL){
    // file not found
    perror("File read error: ");
  } else {
    fgets(noRead, sizeof(noRead), reader); // skip title line
    while(true){
      struct account *loc = (struct account*)malloc(sizeof(struct account));
      if(fscanf(reader, "%d  %lf  %lf", &loc->accountNo, &loc->openingBal, &loc->closingBal) > 0){
        loc->next = NULL;
        //printf("%d  %lf %lf\n", loc->accountNo, loc->openingBal, loc->closingBal);
        if(accountList->head == NULL){
          // if list is empty assign current as head
          accountList->head = loc;
        } else {
          accountList->tail->next = loc;
        }
        accountList->tail=loc; // current is at end of list
      } else {
        free(loc);
        break; // reached EOF
      }
    }
    fclose(reader);
  }

  // 3. Load Client Details
  printf("Reading %s\n", clientFile);
  reader=NULL;
  reader=fopen(clientFile, "r");
  char *val;
  char formats[] = {'\t', ',', '\n'};
  if(reader==NULL){
    // file not found
    perror("File read error: ");
  } else {
    fgets(noRead, sizeof(noRead), reader); // skip title line
    while(fgets(noRead, sizeof(noRead), reader)){
      struct client *loc = (struct client*)malloc(sizeof(struct client));
      int acc[]={0,0,0};

      val = strtok(noRead, "  ");
      strcpy(loc->firstname, val);

      val = strtok(NULL, "  ");
      strcpy(loc->lastname, val);

      val = strtok(NULL, "  ");
      sscanf(val, "%d", &loc->clientNo);

      val = strtok(NULL, ",");
      sscanf(val, "%d", &acc[0]);

      val = strtok(NULL, ",");
      if(val != NULL){
        sscanf(val, "%d", &acc[1]);
      }
      val = strtok(NULL, "\n");
      if(val != NULL){
        sscanf(val, "%d", &acc[2]);
      }

      assignAccounts(loc, acc, accountList->head);

      /*printf("\nClientNo: %d  First: %s Last: %s\n", loc->clientNo, loc->firstname, loc->lastname);
      if(loc->savings != NULL){
        printf("Savings: %d ", loc->savings->accountNo);
      }
      if(loc->credit != NULL){
        printf("Credit: %d ", loc->credit->accountNo);
      }
      if(loc->loan != NULL){
        printf("Loan: %d ", loc->loan->accountNo);
      }*/
      loc->next = NULL;
      if(clientList->head == NULL){
        // if list is empty assign current as head
        clientList->head = loc;
      } else {
        clientList->tail->next = loc;
      }
      clientList->tail=loc; // current is at end of list*/

    }
    fclose(reader);
  }

  // 4. Load Transactions
  printf("Reading %s\n", transactionFile);
  reader=NULL;
  reader=fopen(transactionFile, "r");
  if(reader==NULL){
    // file not found
    perror("File read error: ");
  } else {
    fgets(noRead, sizeof(noRead), reader); // skip title line
    while(!feof(reader)){
      struct transaction *loc = (struct transaction*)malloc(sizeof(struct transaction));
      if(fscanf(reader, "%d %d %d %lf", &loc->fromAccount, &loc->toAccount, &loc->type, &loc->amount) > 0){
        loc->next = NULL;
        //printf("%d  %d  %d  %lf\n", loc->fromAccount, loc->toAccount, loc->type, loc->amount);
        if(transactionList->head == NULL){
          // if list is empty assign current as head
          transactionList->head = loc;
        } else {
          transactionList->tail->next = loc;
        }
        transactionList->tail=loc; // current is at end of list
      } else {
        free(loc);
        break;
      }
    }
    fclose(reader);
  }
}
void assignAccounts(struct client *entry, int *acc, struct account *head){
  entry->savings = NULL;
  entry->loan = NULL;
  entry->credit = NULL;
  for(int i = 0; i < 3; i++){
    if(determineAccount(acc[i]) == 'S' && acc[i] != 0){
      entry->savings = getAccount(acc[i], head);
    } else if(determineAccount(acc[i]) == 'L' && acc[i] != 0){
      entry->loan = getAccount(acc[i], head);
    } else if(determineAccount(acc[i]) == 'C' && acc[i] != 0){
      entry->credit = getAccount(acc[i], head);
    }
  }
}

/*Determines the account type and returns the character corresponding to
that type*/
char determineAccount(int accountNum){
  /*Account types:
  Savings Account (Account Number is a multiple of 11)
  Loan Account (Account Number is multiple of 12)
  Credit Card Account (Account number is multiple of 13)*/
  char acc;
  if((accountNum % 11) == 0){
    // Savings Account
    acc = 'S';
  } else if((accountNum % 12) == 0){
    // Loan Account
    acc = 'L';
  } else if((accountNum % 13) == 0){
    // Credit Card Account
    acc = 'C';
  }
  return acc;
}

// Output method, save the file/s i.e. transations/balance
void save_data(struct account_list *accList, struct transaction_list *transList,
 struct client_list *cliList, struct auth_list *authList){
  /* sort all transactions in the transaction list by from account in ascending
  order*/
  printf("Sorting transactions...");
  sortTransactions(&transList->head);
  printf("Done\n");
  printf("Saving Data...");
  /*write the transactions to Transactions.txt include a header*/
  FILE *tFile = fopen(transactionFile, "wb");
  fprintf(tFile, "FromAccount      ToAccount       TranType    Amount\n");
  struct transaction *traL = transList->head;
  if(tFile != NULL){
  	while(traL != NULL){
  		fprintf(tFile, "%11d%15d%12d%12.2lf\n", traL->fromAccount, traL->toAccount,
  	 traL->type, traL->amount);
  	 traL = traL->next;
  	}
  }
  fclose(tFile);
  /*update the account closing balances in Accounts.txt*/
  FILE *aFile = fopen(accountFile, "wb");
  fprintf(aFile, "AccountNo      OpeningBal     ClosingBal\n");
  struct account *accL = accList->head;
  if(aFile != NULL){
  	while(accL != NULL){
  		fprintf(aFile, "%d%15.2lf%15.2lf\n", accL->accountNo, accL->openingBal,
     accL->closingBal);
  	 accL = accL->next;
  	}
  }
  printf("Done\n");
  printf("Freeing Database Memory...");
  //Transactions
  struct transaction *tHead = transList->head;
  struct transaction *tSwap;

  while(tHead != NULL){
    tSwap = tHead;
    tHead = tHead->next;
    free(tSwap);
  }
  free(transList);
  // Accounts
  struct account *aHead = accList->head;
  struct account *aSwap;

  while(aHead != NULL){
    aSwap = aHead;
    aHead = aHead->next;
    free(aSwap);
  }
  free(accList);
  // clients
  struct client *cHead = cliList->head;
  struct client *cSwap;

  while(cHead != NULL){
    cSwap = cHead;
    cHead = cHead->next;
    free(cSwap);
  }
  free(cliList);
  // Authentication
  struct auth *uHead = authList->head;
  struct auth *uSwap;

  while(uHead != NULL){
    uSwap = uHead;
    uHead = uHead->next;
    free(uSwap);
  }
  free(authList);
  printf("Done\n");
}
void freeTransactions(struct transaction_list *list){
  struct transaction *head = list->head;
  struct transaction *holder;

  while(head != NULL){
    holder = head;
    head = head->next;
    free(holder);
  }
  free(list);
}

/*sort list using merge sort, implementation based on:
http://www.geeksforgeeks.org/merge-sort-for-linked-list/
*/
void sortTransactions(struct transaction** headRef){
  struct transaction* head = *headRef;
  struct transaction* list1;
  struct transaction* list2;

  if(head == NULL || head->next == NULL ){
    return;
  }
  split(head, &list1, &list2);

  sortTransactions(&list1);
  sortTransactions(&list2);

  *headRef = merge(list1, list2);
}

struct transaction* merge(struct transaction* list1, struct transaction* list2){
  struct transaction* merged = NULL;

  if(list1 == NULL){
    return(list2);
  } else if(list2 == NULL){
    return(list1);
  }

  if(list1->fromAccount <= list2->fromAccount){
    merged = list1;
    merged->next = merge(list1->next, list2);
  } else {
    merged = list2;
    merged->next = merge(list1, list2->next);
  }
  return(merged);
}

void split(struct transaction* bodyList, struct transaction** front, struct transaction** back){
  struct transaction* afterMid;
  struct transaction* beforeMid;

  if(bodyList == NULL || bodyList->next == NULL){
    *front = bodyList;
    *back = NULL;
  } else {
    beforeMid = bodyList;
    afterMid = bodyList->next;

    while(afterMid != NULL){
      afterMid = afterMid->next;
      if(afterMid != NULL){
        beforeMid = beforeMid->next;
        afterMid = afterMid->next;
      }
    }

    *front = bodyList;
    *back = beforeMid->next;
    beforeMid->next = NULL;
  }
}
