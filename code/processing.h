/*File containing the methods for processing transactions.
used for interacting with the linked lists that contain working data
for balance, transaction, etc.
Partnered with processing.c*/
#ifndef PROCESSING_H_
#define PROCESSING_H_

struct account * getAccount(int accountNumber, struct account *head);

int deposit(double amount, struct account *depAcc, struct transaction_list *trans);

int withdraw(double amount, struct account *depAcc, int AccountType, struct transaction_list *trans);

void newTransaction(struct transaction_list *list, int from, int to, int type, double amount);

int transfer(double amount, struct account *from, struct account *to, struct transaction_list *trans, int accountType);

#endif // PROCESSING_H_
