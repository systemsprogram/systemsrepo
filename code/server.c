/*Server for the ATM application*/
#define _GNU_SOURCE
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include "fileIO.h"
#include "processing.h"
#include "userFunc.h"

#define BACKLOG 10     /* how many pending connections queue will hold */
#define NUM_HANDLER_THREADS 10 /* The number of threads that can service requests */
pthread_mutex_t client_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
pthread_cond_t got_client = PTHREAD_COND_INITIALIZER;
int num_clients = 0;

#define MAXDATASIZE 256 /* the max message size to the client */
bool volatile run = true; /* SIGINT variable */
char buf[MAXDATASIZE];
const char connectfailure[] = "disconnectClient";
const char loginfailure[] = "badCredentials";
const char doubleSend[] = "finalSend";

struct clientData {
	int socket;
	struct clientData* next;
};

struct clientData* clientHead = NULL;
struct clientData* clientTail = NULL;

/* Lists to store data for the system */
struct auth_list *authList;
struct account_list *accountList;
struct client_list *clientList;
struct transaction_list *transactionList;

void initShutdown(int thing){
	printf("Server is shutting down:\n");
	// do shutdown tasks
	run = false; // stop server loop running
	// save data and free list memory
	save_data(accountList, transactionList, clientList, authList);
	printf("Server is shut down\n");
	exit(0);
}
/* Add a client to the queue to wait for a thread
Creates a client structure and adds it to the list of waiting clients
incrementing the number of waiting client by 1*/
void add_client(int socketNum,
	 pthread_mutex_t *p_mutex,
	  pthread_cond_t *p_cond){
			int code;
			struct clientData *the_client;

			/*Allocate and store the client socket*/
			the_client = (struct clientData*)malloc(sizeof(struct clientData));
			if(!the_client){
				perror("Cannot add another client to the queue out of memory");
				exit(1);
			}
			the_client->socket = socketNum;
			the_client->next = NULL;

			/* lock access to the client list to ensure uninterrupted access */
			code = pthread_mutex_lock(p_mutex);

			/* add the new client to the waiting list for serving */
			if(num_clients == 0){
				clientHead = the_client;
				clientTail = the_client;
			} else {
				clientTail->next = the_client;
				clientTail = the_client;
			}

			/* The number of pending clients has increased*/
			num_clients++;

			code = pthread_mutex_unlock(p_mutex);

			/* Tell the condition variable theres a new client*/
			code = pthread_cond_signal(p_cond);
}

/* get a client from the queue
Returns a pointer to the client struct*/
struct clientData* get_client(pthread_mutex_t *p_mutex){
	int code;
	struct clientData *the_client;

	/* requisition access to the list */
	code = pthread_mutex_lock(p_mutex);

	if(num_clients > 0){
		the_client = clientHead;
		clientHead = the_client->next;
		if(clientHead == NULL){ /* if the next was NULL the list is empty*/
			clientTail = NULL;
		}
		/*A should be decremented from the counter*/
		num_clients--;
	} else {
		/*No clients in the list*/
		the_client = NULL;
	}

	/*finished with the client list*/
	code = pthread_mutex_unlock(p_mutex);

	/* return the client ready for serving*/
	return the_client;
}
/* Serve the dialogs to the client and get responses*/
void serveClient(struct clientData *the_client, int thr_id){
	int new_fd = the_client->socket;
	bool loggedIn = false;
	bool failed = false;
	char endLine[] = "\n\n===========================================\n\n";

	const char welcome[MAXDATASIZE] = "=======================================================\n\n"
	"Welcome to the online ATM system\n\n"
	"=======================================================\n\n"
	"You are required to logon with your registered Username and PIN\n\n"
	"Please enter your username-->";

	send(new_fd, welcome , sizeof(welcome), 0);

	char userN[MAX_SIZE];
	struct auth *user = NULL;
	struct client *name = NULL;

	if(recv(new_fd, userN, sizeof(userN), 0) <= 0) /*check for error */
		failed = true;

	user = getCredentials(userN, authList->head);
	if (user != NULL && !failed) {
		printf("found the username!\n");
		printf("%s\n",user->username);
		const char password[] = "\nPlease enter your password-->";
		send(new_fd, password, sizeof(password), 0);

		char pass[MAX_SIZE];
		if(recv(new_fd, pass, sizeof(pass), 0) <= 0)
			failed = true;

		if(atoi(pass) == user->pin) {
			loggedIn = true;
		}
	}
	if(!loggedIn && !failed){
		printf("user or pass incorrect\n");
		send(new_fd, loginfailure,sizeof(loginfailure), 0);
	}
	while(loggedIn && !failed) {
		name = getDetails(user->clientNo, clientList->head);
		char menu[MAXDATASIZE] = "Welcome to the ATM system\n\n";
		char options[] = "\n\nplease enter a selection:"
		"\n<1> Account Balance"
		"\n<2> Withdrawal"
		"\n<3> Deposit"
		"\n<4> transfer"
		"\n<5> Transaction Listing"
		"\n<6> EXIT"
		"\nselection option 1-6 ->";
		sprintf(menu, "\nYou are Currently logged in as %s %s\nClient number: %d %s", name->firstname, name->lastname, name->clientNo, options);
		char selection[MAX_SIZE];
		char errormsg[MAXDATASIZE] = "\nInvalid selection. Please select option from menu!\n";
		strcat(errormsg, menu);

		bool valid = false;
		bool multi = false;
		while(!valid) {
			if(multi){
				// not the first attempt
				send(new_fd, errormsg, sizeof(errormsg), 0);
			} else {
				send(new_fd, menu, sizeof(menu), 0);
			}
			if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
				failed = true;
				break; /*exit loop if failure of recieve*/
			}
			//printf("RECIEVED: %s\n", selection);
			int select = atoi(selection);
			if(select >= 1 && select <= 6) {
				valid = true;
				//printf("THE SELECTION WAS VALID\n");
			} else {
				multi = true;
				//printf("THE SELECTION WAS INVALID\n");
			}
			if(select == 6){
				loggedIn = false;
				send(new_fd, connectfailure,sizeof(connectfailure), 0);
				break;
			}
			/*create the listing options for account balance, Deposit and
			Transaction Listing*/
			int num = 0;
			int options[3];
			char numStr[] = "";
			char listings[MAXDATASIZE] = "\n\nSelect Account Type\n";
			if(name->savings != NULL){
				sprintf(numStr, "%d", num+1);
				strcat(listings, numStr);
				strcat(listings, ". Savings Account\n");
				num++;
				options[0] = num;
			}
			if(name->loan != NULL){
				sprintf(numStr, "%d", num+1);
				strcat(listings, numStr);
				strcat(listings, ". Loan Account\n");
				num++;
				options[1] = num;
			}
			if(name->credit != NULL){
				sprintf(numStr, "%d", num+1);
				strcat(listings, numStr);
				strcat(listings, ". Credit Card\n");
				num++;
				options[2] = num;
			}
			strcat(listings, "\n\nEnter your selection (E/e to exit) -");

			/*create options for withdrawal */
			int num2 = 0;
			int options2[2];
			char listings2[MAXDATASIZE] = "\n\nSelect Account Type\n";
			if(name->savings != NULL){
				sprintf(numStr, "%d", num2+1);
				strcat(listings2, numStr);
				strcat(listings2, ". Savings Account\n");
				num2++;
				options2[0] = num2;
			}
			if(name->credit != NULL){
				sprintf(numStr, "%d", num2+1);
				strcat(listings2, numStr);
				strcat(listings2, ". Credit Card\n");
				num2++;
				options2[1] = num2;
			}
			char listings1[MAXDATASIZE] = "";
			strcat(listings1, listings2);
			strcat(listings2, "\n\nEnter your selection (E/e to exit) -");

			bool correct = false; /*Whether the correct option was selected*/
			bool first = true; /*Is this the first attempt*/
			int val = 0; /*Value of the selection*/
			char badChoice[MAXDATASIZE] = ""; /*error message*/
			char balanced[MAXDATASIZE] = "";

			if(select == 1){
				/*ACCOUNT BALANCE: Display options for account balance*/
				while(!correct){
					/*send the listings*/
					if(first){
						send(new_fd, listings, sizeof(listings), 0);
						first = false;
					} else {
						sprintf(badChoice, "\nPlease Select a valid option!\n\n");
						strcat(badChoice, listings);
						send(new_fd, badChoice, sizeof(badChoice), 0);
					}
					if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
						failed = true;
						break; /*exit loop if failure of recieve*/
					}
					val = atoi(selection);
					if(val <= num && val >= 1){
						//printf("value correct\n");
						correct = true;
					}
					if(strcmp("e", selection) == 0 || strcmp("E", selection) == 0){
						break;
					}
				}
				if(correct){
					char theName[] = "";
					char accountDetail[] = "";
					strcat(balanced, endLine);

					sprintf(theName, "Account Name - %s %s\n\n", name->firstname, name->lastname);
					strcat(balanced, theName);
					if(options[0] == val && correct){
						/* Show savings account balance*/
						sprintf(accountDetail, "Current Balance for Account %d : $%.2f\n",
					 name->savings->accountNo, name->savings->closingBal);
					 strcat(balanced, accountDetail);
					}
					if(options[1] == val && correct){
						/* Show loan account balance*/
						sprintf(accountDetail, "Current Balance for Account %d : $%.2f\n",
					 name->loan->accountNo, name->loan->closingBal);
					 strcat(balanced, accountDetail);
					}
					if(options[2] == val && correct){
						/* Show credit card balance*/
						sprintf(accountDetail, "Current Balance for Account %d : $%.2f\n",
					 name->credit->accountNo, name->credit->closingBal);
					 strcat(balanced, accountDetail);
					}
					strcat(balanced, endLine);
					bool done = false;
					char cond[] = "\nEnter (e) to return ->";
					strcat(balanced, cond);
					send(new_fd, balanced, sizeof(balanced), 0);
					while(!done){
						if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
							failed = true;
							break; /*exit loop if failure of recieve*/
						}
						if(strcmp(selection, "e") == 0){
							done = true;
						} else {
							send(new_fd, cond, sizeof(cond), 0);
						}
					}
					valid = false;
					break;
				}
			}
			if(select == 2){
				/*WITHDRAWAL: Display options for withdrawal*/
				while(!correct){
					/*send the listings*/
					if(first){
						send(new_fd, listings2, sizeof(listings), 0);
						first = false;
					} else {
						sprintf(badChoice, "\nPlease Select a valid option!\n\n");
						strcat(badChoice, listings2);
						send(new_fd, badChoice, sizeof(badChoice), 0);
					}
					if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
						failed = true;
						break; /*exit loop if failure of recieve*/
					}
					val = atoi(selection);
					if(val <= num2 && val > 0){
						//printf("value correct\n");
						correct = true;
					}
					if(strcmp("e", selection) == 0 || strcmp("E", selection) == 0){
						break;
					}
				}
				if(correct){
					char accountDetail[] = "";
					char dialog[] = "\nEnter the amount to withdraw (E/e to exit) : $";
					double am = 0.0;

					if(options2[0] == val && correct){
						/* Withdraw from savings account*/
						send(new_fd, dialog, sizeof(dialog), 0);
						if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
							failed = true;
							break; /*exit loop if failure of recieve*/
						}
						if(strcmp(selection, "e") == 0 || strcmp(selection, "E") == 0){
							break;
						}
						am = atof(selection);

						if(withdraw(am, name->savings, 1, transactionList) == 1 && am > 0.0){
							sprintf(accountDetail, "\n\nWithdrawal Completed: Closing Balance $%.2f\n",
							 name->savings->closingBal);
						} else {
							sprintf(accountDetail, "\n\nInsufficient Funds - Unable to process request\n");

						}
					 strcat(balanced, accountDetail);
					}
					if(options[1] == val && correct){
						/* Withdraw from credit account*/
						send(new_fd, dialog, sizeof(dialog), 0);
						if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
							failed = true;
							break; /*exit loop if failure of recieve*/
						}
						if(strcmp(selection, "e") == 0 || strcmp(selection, "E") == 0){
							break;
						}
						am = atof(selection);

						if(withdraw(am, name->credit, 2, transactionList) == 1){
							sprintf(accountDetail, "\n\nWithdrawal Completed: Closing Balance $%.2f\n\n",
						 name->credit->closingBal);
						} else {
							sprintf(accountDetail, "\n\nInsufficient Funds - Unable to process request\n\n");
						}

					 strcat(balanced, accountDetail);
					}
					strcat(balanced, endLine);
					bool done = false;
					char cond[] = "\nEnter (e) to return ->";
					strcat(balanced, cond);
					send(new_fd, balanced, sizeof(balanced), 0);
					while(!done){
						if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
							failed = true;
							break; /*exit loop if failure of recieve*/
						}
						if(strcmp(selection, "e") == 0){
							done = true;
						} else {
							send(new_fd, cond, sizeof(cond), 0);
						}
					}
					valid = false;
					break;
				}
			}
			if(select == 3){
				/*DEPOSIT: Display options for Deposit*/
				while(!correct){
					/*send the listings*/
					char warning[MAXDATASIZE] = "\n\nThe maximum daily deposit is $1000!\n\n";
					strcat(warning, listings);
					if(first){
						send(new_fd, warning, sizeof(listings), 0);
						first = false;
					} else {
						sprintf(badChoice, "\nPlease Select a valid option!\n\n");
						strcat(badChoice, listings);
						send(new_fd, badChoice, sizeof(badChoice), 0);
					}
					if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
						failed = true;
						break; /*exit loop if failure of recieve*/
					}
					val = atoi(selection);
					if(val <= num && val >= 1){
						//printf("value correct\n");
						correct = true;
					}
					if(strcmp("e", selection) == 0 || strcmp("E", selection) == 0){
						break;
					}
				}
				if(correct){
					char theName[] = "";
					char accountDetail[] = "";

					struct account *selected = NULL;
					if(options[0] == val && correct){
						selected = name->savings;
					}
					if(options[1] == val && correct){
						selected = name->loan;
					}
					if(options[2] == val && correct){
						selected = name->credit;
					}
					if(selected != NULL){
						bool done = false;
						bool first = true;
						double am = 0.0;
						char overOneThousand[MAXDATASIZE] = "\n\nYou cannot deposit more than $1000 in a single transaction!\n\n";
						char depositMessage[MAXDATASIZE] = "\n\nEnter the amount to deposit (E/e to exit) : $";
						while(!done){
							if(first){
								send(new_fd, depositMessage, sizeof(depositMessage), 0);
								first = false;
							} else {
								sprintf(depositMessage, "\nEnter the amount to deposit (E/e to exit) : $");
								strcat(overOneThousand, depositMessage);
								send(new_fd, overOneThousand, sizeof(overOneThousand), 0);
							}
							if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
								failed = true;
								break; /*exit loop if failure of recieve*/
							}
							if(strcmp(selection, "e") == 0 || strcmp(selection, "E") == 0){
								break;
							}
							am = atof(selection);
							if(am <= 1000.00){
								//printf("value correct\n");
								done = true;
								deposit(am, selected, transactionList);
							}
						}
						if(done){
							sprintf(accountDetail, "\nDeposit Completed: Closing Balance : $%.2f\n",
						 selected->closingBal);
						 strcat(balanced, accountDetail);
						}
					}
					strcat(balanced, endLine);
					bool done = false;
					char cond[] = "\nEnter (e) to return ->";
					strcat(balanced, cond);
					send(new_fd, balanced, sizeof(balanced), 0);
					while(!done){
						if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
							failed = true;
							break; /*exit loop if failure of recieve*/
						}
						if(strcmp(selection, "e") == 0){
							done = true;
						} else {
							send(new_fd, cond, sizeof(cond), 0);
						}
					}
					valid = false;
					break;
				}
			}
			if(select == 4){
				/*TRANSFER: Display options for Transfer*/
				char theFrom[MAXDATASIZE] = "";
				char theTo[MAXDATASIZE] = "";
				int fromType = 0;
				strcat(theFrom, listings1);
				strcat(theFrom, "\n\nSelect Account to Transfer From (E/e to exit) -");
				while(!correct){
					/*send the listings*/
					if(first){
						send(new_fd, theFrom, sizeof(theFrom), 0);
						first = false;
					} else {
						sprintf(badChoice, "\nPlease Select a valid option!\n\n");
						strcat(badChoice, theFrom);
						send(new_fd, badChoice, sizeof(badChoice), 0);
					}
					if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
						failed = true;
						break; /*exit loop if failure of recieve*/
					}
					val = atoi(selection);
					if(val <= num2 && val >= 1){
						//printf("value correct\n");
						correct = true;
					}
					if(strcmp("e", selection) == 0 || strcmp("E", selection) == 0){
						break;
					}
				}
				if(correct){
					struct account *from = NULL;
					int select = 0;
					int avail[4] = {0, 0, 0, 0};
					if(options2[0] == val && correct){
						/* transfer from savings account*/
						from = name->savings;
						avail[0] = 0;
						fromType = 1;
					}
					if(options[1] == val && correct){
						/* transfer from credit account*/
						from = name->credit;
						avail[1] = 0;
						fromType = 2;
					}
					sprintf(theTo, "\n\nSelect Account Type\n");
					char temp[MAXDATASIZE] = "";
					if(name->loan != NULL){
						select++;
						sprintf(temp, "%d. Loan Account\n", select);
						strcat(theTo, temp);
						avail[0] = select;
					}
					if(name->savings != NULL && avail[1] > 0){
						select++;
						sprintf(temp, "%d. Savings Account\n", select);
						strcat(theTo, temp);
						avail[1] = select;
					}
					if(name->credit != NULL && avail[2] > 0){
						select++;
						sprintf(temp, "%d. Credit Card\n", select);
						strcat(theTo, temp);
						avail[2] = select;
					}
					select++;
					avail[3] = select;
					sprintf(temp, "%d. External Account\n", select);
					strcat(theTo, temp);
					strcat(theTo, "\nSelect Account to Transfer To (E/e to exit) - ");

					bool another = false;
					first = true;
					val = 0;
					while(!another){
						/*send the listings*/
						if(first){
							send(new_fd, theTo, sizeof(theTo), 0);
							first = false;
						} else {
							sprintf(badChoice, "\nPlease Select a valid option!\n\n");
							strcat(badChoice, theTo);
							send(new_fd, badChoice, sizeof(badChoice), 0);
						}
						if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
							failed = true;
							break; /*exit loop if failure of recieve*/
						}
						val = atoi(selection);
						if(val <= select && val >= 1){
							//printf("value correct\n");
							another = true;
						}
						if(strcmp("e", selection) == 0 || strcmp("E", selection) == 0){
							break;
						}
					}
					struct account *to = NULL;
					bool external = false;
					if(avail[0] == val && another){
						/*Loan account*/
						to = name->loan;
					}
					if(avail[1] == val && another){
						/*savings account*/
						to = name->savings;
					}
					if(avail[2] == val && another){
						/*credit card*/
						to = name->credit;
					}
					if(avail[3] == val && another){
						/*External Transfer*/
						char desMsg[MAXDATASIZE] = "\n\nEnter Destination Account Number (E/e to exit) - ";
						bool isAccount = false;
						first = true;
						while(!isAccount){
							if(first){
								send(new_fd, desMsg, sizeof(desMsg), 0);
								first = false;
							} else {
								sprintf(badChoice, "\n\nInvalid Destination Account Number - Try Again");
								strcat(badChoice, desMsg);
								send(new_fd, badChoice, sizeof(badChoice), 0);
							}
							if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
								failed = true;
								break; /*exit loop if failure of recieve*/
							}
							val = atoi(selection);
							to = getAccount(val, accountList->head);
							if(to != NULL){
								external = true;
								isAccount = true;
							}
							if(strcmp("e", selection) == 0 || strcmp("E", selection) == 0){
								break;
							}
						}
					}
					if(from != NULL && to != NULL){
						/*ask for transfer amount*/
						first = true;
						bool done = false;
						double am = 0.0;
						char amtMsg[] = "\n\nEnter the amount to transfer (E/e to exit) : $";
						while(!done){
							if(first){
								send(new_fd, amtMsg, sizeof(amtMsg), 0);
								first = false;
							} else {
								sprintf(badChoice, "\n\nInvalid Amount - Enter a Valid Amount to Transfer.");
								strcat(badChoice, amtMsg);
								send(new_fd, badChoice, sizeof(badChoice), 0);
							}
							if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
								failed = true;
								break; /*exit loop if failure of recieve*/
							}
							if(strcmp(selection, "e") == 0 || strcmp(selection, "E") == 0){
								break;
							}
							am = atof(selection);
							if(transfer(am, from, to, transactionList, fromType) > 0){
								done = true;
							}
						}
						bool end = false;
						char fLine[MAXDATASIZE] = "";
						char confMsg[MAXDATASIZE] = "";
						char tLine[MAXDATASIZE] = "";
						sprintf(fLine, "Deducted $%.2f From: Account %d - Closing Balance - $%.2f\n",
						am, from->accountNo, from->closingBal);

						if(external){
							strcat(confMsg, "\nEXTERNAL TRANSFER\n\n");
							strcat(confMsg, fLine);
							sprintf(tLine, "Transfer $%.2f Dest: Account %d\n\n",
							am, to->accountNo);
							strcat(confMsg, tLine);
						} else {
							strcat(confMsg, "\nINTERNAL TRANSFER\n\n");
							strcat(confMsg, fLine);
							sprintf(tLine, "Transfer $%.2f Dest: Account %d - Closing Balance - $%.2f\n\n",
							am, to->accountNo, to->closingBal);
							strcat(confMsg, tLine);
						}

						char cond[] = "\nEnter (e) to return ->";
						strcat(confMsg, endLine);
						strcat(confMsg, cond);

						send(new_fd, confMsg, sizeof(confMsg), 0);
						end = false;
						while(!end){
							if(recv(new_fd, selection, sizeof(selection), 0) <= 0){
								failed = true;
								break; /*exit loop if failure of recieve*/
							}
							if(strcmp(selection, "e") == 0){
								end = true;
							} else {
								send(new_fd, cond, sizeof(cond), 0);
							}
						}
					}
					valid = false;
					break;
				}
			}
			if(select == 5){
				/*TRANSACTION LISTING: Display options for Transaction Listing*/
				printf("NOT YET IMPLEMENTED");
			}
		}
	}
	printf("Close client socket\n");
	if(!failed)
		send(new_fd, connectfailure,sizeof(connectfailure), 0);
	close(new_fd);
}

/* method to service a request to a socket when thread available*/
void* serviceLoop(void *pass){
	int code;
	struct clientData *the_client;
	int thr_id = *((int*)pass);

	/* lock the list */
	code = pthread_mutex_lock(&client_mutex);

	/* get thread to loop through and wait to serve a client */
	/* Keep checking while the server is up*/
	while(run){
		if(num_clients > 0){
			the_client = get_client(&client_mutex);
			if(the_client){ /*if a client is returned*/
				code = pthread_mutex_unlock(&client_mutex);
				serveClient(the_client, thr_id);
				free(the_client);
				/*lock the mutex and repeat*/
				code = pthread_mutex_lock(&client_mutex);
			}
		} else {
			/*there are currently no available clients, wait for a client*/
			code = pthread_cond_wait(&got_client, &client_mutex);
		}
	}
	/* End the thread the main thread is exiting*/
	pthread_exit(0);
}

int main(int argc, char *argv[])
{
	signal(SIGINT, initShutdown);
  // Read in the files
	authList = (struct auth_list*)malloc(sizeof(struct auth_list));
	accountList = (struct account_list*)malloc(sizeof(struct account_list));
	clientList = (struct client_list*)malloc(sizeof(struct client_list));
	transactionList = (struct transaction_list*)malloc(sizeof(struct transaction_list));

	load_data(authList, accountList, clientList, transactionList);

	int sockfd, new_fd;  /* listen on sock_fd, new connection on new_fd */
	struct sockaddr_in my_addr;    /* my address information */
	struct sockaddr_in their_addr; /* connector's address information */
	socklen_t sin_size;

	int port = 12345; //default port
	if(argc >= 2){
		//check argc is an int
		port = atoi(argv[1]);
	}

	if(port <= 0 || port > 65535){
		fprintf(stderr, "invalid port \n");
	}

	/* generate the socket */
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket allocation error\n");
		int enable = 1;
		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
			perror("failed to reuse the socket\n");
			exit(1);
		}
	}

	/* generate the end point */
	my_addr.sin_family = AF_INET;         /* host byte order */
	my_addr.sin_port = htons(port);     /* short, network byte order */
	my_addr.sin_addr.s_addr = INADDR_ANY; /* auto-fill with my IP */

	/* bind the socket to the end point */
	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) \
	== -1) {
		perror("bind");
		exit(1);
	}

  /* create the threads for the thread pool */
	int thr_id[NUM_HANDLER_THREADS];
	pthread_t p_threads[NUM_HANDLER_THREADS];

	for(int i = 0; i < NUM_HANDLER_THREADS; i++){
		thr_id[i] = i;
		pthread_create(&p_threads[i], NULL, serviceLoop, (void*)&thr_id[i]);
	}

	/* start listnening */
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	printf("Server is listnening on port %d...\n", port);

	/* repeat: accept, queue, serve, close connection */
	while(run) {  /* accept()*/

    // handle requests with thread pool instead of fork below
    // one thread for one client for a max of 10 clients

		sin_size = sizeof(struct sockaddr_in);
		if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, \
		&sin_size)) == -1) {
			perror("accept");
			continue;
		}
		printf("server: got connection from %s\n", \
		inet_ntoa(their_addr.sin_addr));
		add_client(new_fd, &client_mutex, &got_client);
	}
}
