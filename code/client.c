#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>
#include <netdb.h>
#include <signal.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>

#define MAXDATASIZE 256 /* max number of bytes we can get at once */
#define MAX_SIZE 40 /* max size string to send to the server*/

int main(int argc, char *argv[])
{
	signal(SIGINT, NULL);

	int sockfd, numbytes;
	char buf[MAXDATASIZE];
	struct hostent *he;
	struct sockaddr_in their_addr; /* connector's address information */

	if (argc < 2) {
		fprintf(stderr,"usage: client hostname\n");
		exit(1);
	}
	int port = atoi(argv[2]);
	if(port <= 0 || port > 65535){
		fprintf(stderr, "invalid port \n");
	}

	if ((he=gethostbyname(argv[1])) == NULL) {  /* get the host info */
		herror("gethostbyname");
		exit(1);
	}

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	their_addr.sin_family = AF_INET;      /* host byte order */
	their_addr.sin_port = htons(port);    /* short, network byte order */
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8);     /* zero the rest of the struct */

	if (connect(sockfd, (struct sockaddr *)&their_addr, \
	sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
	}
	printf("Wating for slot on server...\n");

	buf[numbytes] = '\0';

bool running = true;
while(running) {
	if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
		perror("recv");
		exit(1);
	}
	if (strcmp(buf, "disconnectClient") == 0){
		break;
	}
	if(strcmp(buf, "badCredentials") == 0){
		printf("Incorrect account information provided\n");
		break;
	}
	if(strcmp(buf, "finalSend") == 0){
		printf("%s", buf);
		if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
			perror("recv");
			exit(1);
		}
		printf("%s\n",buf);
		if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
			perror("recv");
			exit(1);
		}
		printf("%s\n",buf);
		if ((numbytes=recv(sockfd, buf, MAXDATASIZE, 0)) == -1) {
			perror("recv");
			exit(1);
		}
	}
	char input[MAX_SIZE];
	printf("%s",buf);
	scanf("%s", input);
	send(sockfd, input, sizeof(input), 0);
	//printf("%s\n", input);

}
printf("Disconnecting from server\n");
close(sockfd);

return 0;
}
