/*This header file contains the function definitions for verifying the users access and
any other function as mirrored in userFunc.c*/
#ifndef USERFUNC_H_
#define USERFUNC_H_
#include "fileIO.h"

struct auth * getCredentials(char user[MAX_SIZE], struct auth *head);

struct client * getDetails(int num, struct client *head);


#endif // USERFUNC_H_
