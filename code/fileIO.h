#ifndef FILEIO_H_
#define FILEIO_H_

#define MAX_SIZE 40 // string array size

struct client_list{
  struct client *head;
  struct client *tail;
};
struct auth_list{
  struct auth *head;
  struct auth *tail;
};
struct account_list{
  struct account *head;
  struct account *tail;
};
struct transaction_list{
  struct transaction *head;
  struct transaction *tail;
};

/*from Client_Details.txt */
struct client{
  char firstname[MAX_SIZE];
  char lastname[MAX_SIZE];
  int clientNo;

  // value of account numbers is null if no account
  struct account *savings;
  struct account *credit;
  struct account *loan;

  struct client *next;
};
/*from Authentication.txt*/
struct auth{
  char username[MAX_SIZE];
  int pin;
  int clientNo;

  struct auth *next;
};
/*from Accounts.txt*/
struct account{
  int accountNo;
  double openingBal;
  double closingBal;

  struct account *next;
};
/*from Transactions.txt*/
struct transaction{
  int fromAccount;
  int toAccount;
  int type;
  double amount;

  struct transaction *next;
};

void load_data(struct auth_list *authList, struct account_list *accountList, struct client_list *clientList, struct transaction_list *transactionList);

void save_data(struct account_list *accList, struct transaction_list *transList,
 struct client_list *cliList, struct auth_list *authList);

char determineAccount(int accountNum);

#endif // FILEIO_H_
