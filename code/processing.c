/*File containing the methods for processing transactions.
used for interacting with the linked lists that contain working data
for balance, transaction, etc.*/
#include "fileIO.h"
#include "processing.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/*Method for finding and returning an account pointer by number,
or returns null if account doesnt exist*/
struct account * getAccount(int accountNumber, struct account *head){
  // iterate through list to find number'
  struct account *temp;
  temp = head;
  while(temp != NULL){
    if(temp->accountNo == accountNumber){
      return(temp);
    }
    temp = temp->next;
  }
  return(NULL);
}



/*method for making a deposit for given account
Deposits available for Savings, Loan and Credit
1 returned if successful
-1 Deposit amount is too high ($1000 limit)
Amount value must be positive*/
int deposit(double amount, struct account *depAcc, struct transaction_list *trans){
  if(amount <= 1000 && amount > 0){
    depAcc->closingBal += amount;
    newTransaction(trans, depAcc->accountNo, depAcc->accountNo, 2, amount);
    return 1;
  } else {
    return -1;
  }
}

/*Method for making a withdrawal for given account
Withdrawals only available from (1)savings and (2)credit
1 returned if successful
-1 insufficient funds
Amount to be withdrawed should be positive*/
int withdraw(double amount, struct account *witAcc, int accountType, struct transaction_list *trans){

  if(accountType == 1 && amount <= witAcc->closingBal && amount > 0){
    // savings withdrawal must be = to or < current balance
    witAcc->closingBal -= amount;
    newTransaction(trans, witAcc->accountNo, witAcc->accountNo, 3, -amount);
    return 1;
  } else if(accountType == 2 && witAcc->closingBal - amount > -5000 && amount > 0) {
    // credit card must not go below a balance of -$5000
    witAcc->closingBal -= amount;
    newTransaction(trans, witAcc->accountNo, witAcc->accountNo, 3, -amount);
    return 1;
  }  else {
    return -1;
  }
}

/*Method to transfer between two accounts
1. Clients Can transfer from accounts they own
2. From account must be savings or credit
3. Accounts must exist
4. From account must have sufficient funds or -1 is returned
Transaction type is 4
Amount to be transferred should be positive*/
int transfer(double amount, struct account *from, struct account *to, struct transaction_list *trans, int accountType){
  int success = -1;
  if(accountType == 1 && amount <= from->closingBal && amount > 0){
    // savings withdrawal must be = to or < current balance
    from->closingBal -= amount;
    to->closingBal += amount;
    success = 1;
  } else if(accountType == 2 && from->closingBal - amount > -5000 && amount > 0) {
    // credit card must not go below a balance of -$5000
    from->closingBal -= amount;
    to->closingBal += amount;
    success = 1;
  }

  if(success > 0){
    /*create two transaction upon successful transfer
    From account has a NEGATIVE amount*/
    newTransaction(trans, from->accountNo, to->accountNo, 4, -amount);
    /*to account has a POSITIVE amount*/
    newTransaction(trans, from->accountNo, to->accountNo, 4, amount);
  }
  return success;
}

/*method to create new transaction record after successful transaction*/
void newTransaction(struct transaction_list *list, int from, int to, int type, double amount){
  struct transaction *tran = (struct transaction*)malloc(sizeof(struct transaction));
  tran->next = NULL;
  tran->fromAccount = from;
  tran->toAccount = to;
  tran->type = type;
  tran->amount = amount;

  if(list->head == NULL){
    list->head = tran;
  } else {
    list->tail->next = tran;
  }
  list->tail = tran;
}
